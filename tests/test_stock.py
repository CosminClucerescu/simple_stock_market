from unittest import TestCase

from datetime import datetime, timedelta

from simple_stock_market.stock import CommonStock, PreferredStock, StockList, StockListError
from simple_stock_market.trade import Trade


class TestStockList(TestCase):
    def setUp(self):
        self.stock_list = StockList()

    def test_no_duplicate_symbols(self):
        stock = CommonStock("TST", 100, 100)
        self.stock_list.add(stock)
        stock2 = CommonStock("TST", 100, 100)
        self.assertRaisesRegexp(StockListError, "Stock with symbol TST already present", self.stock_list.add, stock2)

    def test_get_all_share_index(self):
        stock = CommonStock("TST", 100, 100)
        stock.add_trade(Trade(5, 5, "Buy", datetime.now()))
        stock2 = CommonStock("TST2", 100, 100)
        stock2.add_trade(Trade(5, 5, "Buy", datetime.now()))
        self.stock_list.add(stock)
        self.stock_list.add(stock2)
        self.assertEqual(5, self.stock_list.get_gbce_all_share_index())

    def test_get_all_share_index_zero_division(self):
        # pylint: disable=invalid-name
        self.assertEqual(0, self.stock_list.get_gbce_all_share_index())


class TestCommonStock(TestCase):
    def setUp(self):
        self.stock = CommonStock("TST", 8, 100)

    def test_dividend_ratio(self):
        self.assertEqual(0.8, self.stock.get_dividend_yield(10))

    def test_dividend_ratio_zero_division(self):
        # pylint: disable=invalid-name
        self.assertEqual(0, self.stock.get_dividend_yield(0))

    def test_p_e_ratio(self):
        self.assertEqual(0.5, self.stock.get_p_e_ratio(4))

    def test_p_e_ratio_zero_division(self):
        self.stock.last_dividend = 0
        self.assertEqual(0, self.stock.get_p_e_ratio(10))

    def test_add_trade(self):
        expected = [Trade(index + 1, index + 1, "Buy", datetime.now() - timedelta(days=index)) for index in range(10)]
        for trade in expected:
            self.stock.add_trade(trade)
        self.assertEqual(expected, self.stock.trades)

    def test_add_trade_reverse_order(self):
        expected = [Trade(index + 1, index + 1, "Buy", datetime.now() - timedelta(days=index)) for index in range(10)]
        for trade in reversed(expected):
            self.stock.add_trade(trade)
        self.assertEqual(expected, self.stock.trades)

    def test_get_volume_weighted_price(self):
        now = datetime.utcnow()
        trades = [Trade(index + 1, index + 1, "Buy", now - timedelta(minutes=index)) for index in range(20)]
        for trade in trades:
            self.stock.add_trade(trade)
        self.assertEqual(3, self.stock.get_volume_weighted_price(timedelta(minutes=4)))

    def test_get_volume_weighted_price_no_trades(self):
        # pylint: disable=invalid-name
        self.assertEqual(0, self.stock.get_volume_weighted_price(timedelta(days=10)))


class TestPreferredStock(TestCommonStock):
    def setUp(self):
        self.stock = PreferredStock("TST", 8, 100, 0.02)

    def test_dividend_ratio(self):
        self.assertEqual(0.5, self.stock.get_dividend_yield(4))

    def test_dividend_ratio_zero_division(self):
        # pylint: disable=invalid-name
        self.assertEqual(0, self.stock.get_dividend_yield(0))
