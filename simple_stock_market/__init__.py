from simple_stock_market.simple_cli import SimpleCli


def main():
    """
    Entry point.
    :return:
    """
    SimpleCli().run()


if __name__ == "__main__":
    main()
