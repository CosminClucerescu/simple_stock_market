"""
Module for the Stock classes.
"""
from __future__ import division

from simple_stock_market.trade import TradeList


class StockError(Exception):
    pass


class StockListError(Exception):
    pass


class StockList(object):
    def __init__(self):
        self._stock_dict = {}

    @property
    def as_dict(self):
        return dict(self._stock_dict)

    def add(self, stock):
        """
        Add a stock to this StockList, if there's already a stock with the same symbol present on the StockList an
        StockListError will be raised.
        :param stock:
        :type stock: BaseStock
        :return:
        """
        if stock.symbol in self._stock_dict:
            raise StockListError("Stock with symbol {} already present".format(stock.symbol))
        self._stock_dict[stock.symbol] = stock

    def get_gbce_all_share_index(self):
        prod = 1
        count = 0
        for stock in self._stock_dict.values():
            for trade in stock.trades:
                prod *= trade.price
                count += 1
        if not count:
            return 0
        return prod ** (1 / count)


class _BaseStock(object):
    """
        Base Stock class.
    """

    def __init__(self, symbol, last_dividend, par_value):
        self._symbol = symbol
        self._last_dividend = last_dividend
        self._par_value = par_value
        self._trades = TradeList()

    @property
    def trades(self):
        """
        List of trades for this stock, sorted based on trade time, recent trades first.
        :return:
        """
        return self._trades.to_list()

    @property
    def symbol(self):
        return self._symbol

    @property
    def last_dividend(self):
        return self._last_dividend

    @last_dividend.setter
    def last_dividend(self, value):
        self._last_dividend = value

    def add_trade(self, trade):
        """
        :param trade:
        :type trade: trade.Trade
        :return:
        """
        self._trades.add(trade)

    def get_dividend_yield(self, price):
        raise NotImplementedError()

    @staticmethod
    def _get_total_cost_for_trades(trades):
        total = 0
        for trade in trades:
            total += trade.price * trade.shares
        return total

    @staticmethod
    def _get_total_shares_for_trades(trades):
        total = 0
        for trade in trades:
            total += trade.shares
        return total

    def get_volume_weighted_price(self, time_delta):
        """
        Get the volume weighted stock price for trades in the past time_delta time period.
        :type time_delta: datetime.time_delta
        :return:
        """
        trades = self._trades.get_trades_in_past_delta(time_delta)
        if not trades:
            return 0
        total_shares = trades.get_total_shares_quantity()
        if not total_shares:
            return 0
        return trades.get_total_cost() / total_shares

    def get_p_e_ratio(self, price):
        if not self._last_dividend:
            return 0
        return price / self._last_dividend


class CommonStock(_BaseStock):
    """
        Class for stocks of type Common.
    """
    _type = "Common"

    def get_dividend_yield(self, price):
        if not price:
            return 0
        return self._last_dividend / price


class PreferredStock(_BaseStock):
    """
        Class for stocks of type Preferred.
    """
    _type = "Preferred"

    def __init__(self, symbol, last_dividend, par_value, fixed_dividend):
        super(PreferredStock, self).__init__(symbol, last_dividend, par_value)
        if fixed_dividend >= 1 or fixed_dividend < 0:
            raise StockError("Fixed dividend must be in range 0,1")
        self._fixed_dividend = fixed_dividend

    def get_dividend_yield(self, price):
        if not price:
            return 0
        return self._fixed_dividend * self._par_value / price
