from datetime import datetime


class TradeError(Exception):
    pass


class TradeList(object):
    def __init__(self):
        self._trades = []  # sorted based on trade time, recent trades first.

    def to_list(self):
        return list(self._trades)

    def add(self, trade):
        """

        :param trade:
        :type trade: Trade
        :return:
        """
        for index, existing_trade in enumerate(self._trades):
            if existing_trade.timestamp < trade.timestamp:
                # keep the trade list sorted based on the timestamp, more recent dates first.
                self._trades.insert(index, trade)
                return
        self._trades.append(trade)

    def get_trades_in_past_delta(self, time_delta):
        """
        Get a list of trades that were done in the past time_delta time period.
        :param time_delta:
        :type time_delta: datetime.time_delta
        :return:
        :rtype: TradeList
        """
        now = datetime.utcnow()
        trades = TradeList()
        for trade in self._trades:
            if trade.timestamp < now - time_delta:
                break
            trades.add(trade)
        return trades

    def get_total_cost(self):
        """
        Get the total cost for trades in this TradeList.
        Calculated as sum of price*quantity for each trade in thi list.
        :return:
        """
        return sum([trade.price * trade.shares for trade in self._trades])

    def get_total_shares_quantity(self):
        """
        Get the total shares quantity
        :return:
        """
        return sum([trade.shares for trade in self._trades])


class Trade(object):
    def __init__(self, shares, price, type_, timestamp):
        """
        :param shares: Must be greater than 0, TradeError will be raised otherwise.
        :param price:
        :param type_: "Buy" or "Sell", TradeError will be raised otherwise.
        :param timestamp: Assumed to be on utc timezone.
        """
        if shares <= 0:
            raise TradeError("Invalid shares quantity must be greater than 0.")
        self._shares = shares
        self._price = price
        self._timestamp = timestamp
        if type_ not in ["Buy", "Sell"]:
            raise TradeError("Trade type must be either Buy or Sell")
        self._type = type_

    @property
    def price(self):
        return self._price

    @property
    def shares(self):
        return self._shares

    @property
    def timestamp(self):
        return self._timestamp
