"""
Simple stock market.
"""
from __future__ import print_function

from datetime import datetime, timedelta

from simple_stock_market.stock import StockList, CommonStock, PreferredStock, StockListError
from simple_stock_market.trade import Trade, TradeError


class _StopError(Exception):
    pass


class _NoStockError(Exception):
    """Used to stop further input from user in case no stocks are available."""
    pass


class _CliOption(object):
    # pylint: disable=too-few-public-methods
    def __init__(self, text, callback):
        self._text = text
        self._callback = callback

    @property
    def text(self):
        return self._text

    def __str__(self):
        return self._text

    def run(self):
        self._callback()


class _GetInput(object):
    """
        Helper class to get input from user.
    """

    @staticmethod
    def get_int(message):
        while True:
            try:
                return int(input(message))
            except ValueError:
                print("Invalid value, please enter an integer.")

    @staticmethod
    def get_symbol():
        return input("Enter stock symbol: ")

    def select_option(self, options):
        while True:
            try:
                return options[self.get_int("Select option: ")]
            except IndexError:
                print("Invalid option given.")

    def get_dividend(self):
        return self.get_int("Enter stock last dividend: ")

    def get_par_value(self):
        return self.get_int("Enter stock Par Value: ")

    @staticmethod
    def get_fixed_dividend():
        while True:
            try:
                dividend = float(input("Enter fixed dividend: "))
                if dividend < 0 or dividend > 1:
                    raise ValueError()
            except ValueError:
                print("Invalid value, please enter a value between 0 and 1")

    def select_stock(self, stock_list):
        """
        Ask the user to select a Stock from the given stock_list.
        :return:
        :rtype: stock.BaseStock
        """
        if not stock_list.as_dict:
            print("No stocks available add a stock first.")
            raise _NoStockError()
        print("Available stocks: ")
        print("\n".join(stock_list.as_dict.keys()))
        while True:
            symbol = self.get_symbol()
            try:
                return stock_list.as_dict[symbol]
            except KeyError:
                print("Invalid stock symbol")

    def get_price(self):
        return self.get_int("Enter price: ")

    @staticmethod
    def get_timestamp():
        return datetime.utcnow()

    def get_trade(self):
        return Trade(self.get_int("Shares Quantity: "), self.get_price(), input("Type Buy/Sell"),
                     self.get_timestamp())

    @staticmethod
    def get_time_delta():
        return timedelta(minutes=15)


class SimpleCli(object):
    # pylint: disable=too-few-public-methods
    def __init__(self):
        self._stock_list = StockList()
        self._input = _GetInput()
        self._options = [_CliOption("Exit", self._stop),
                         _CliOption("Add stock", self._add_stock),
                         _CliOption("Calculate dividend yield", self._calculate_dividend_yield),
                         _CliOption("Calculate P/E ratio", self._calculate_pe_ratio),
                         _CliOption("Add trade", self._add_trade),
                         _CliOption("Calculate Volume Weighted Stock Price for trades in past 15 minutes.",
                                    self._get_volume_weighted_stock_price),
                         _CliOption("Calculate the GBCE All Share Index", self._get_gbce_all_share_index)]

    def _stop(self):  # pylint: disable=no-self-use
        raise _StopError()

    def _add_stock(self):
        while True:
            type_ = input("Stock type Common/Preferred: ").strip()
            if type_ == "Common":
                self._stock_list.add(
                    CommonStock(self._input.get_symbol(), self._input.get_dividend(), self._input.get_par_value()))
                return
            elif type_ == "Preferred":
                self._stock_list.add(
                    PreferredStock(self._input.get_symbol(), self._input.get_dividend(), self._input.get_par_value(),
                                   self._input.get_fixed_dividend()))
                return
            print("Invalid stock type.")

    def _add_trade(self):
        self._input.select_stock(self._stock_list).add_trade(self._input.get_trade())

    def _print_options(self):
        print("Option list: ")
        for index, option in enumerate(self._options):
            print(index, option)

    def _calculate_dividend_yield(self):
        print("Dividend price: {}".format(
            self._input.select_stock(self._stock_list).get_dividend_yield(self._input.get_price())))

    def _calculate_pe_ratio(self):
        print("P/E ratio: {}".format(self._input.select_stock(self._stock_list).get_p_e_ratio(self._input.get_price())))

    def _get_volume_weighted_stock_price(self):
        print("Volume Weighted Stock Price: {}".format(
            self._input.select_stock(self._stock_list).get_volume_weighted_price(self._input.get_time_delta())))

    def _get_gbce_all_share_index(self):
        print("GBCE All Share Index: {}".format(self._stock_list.get_gbce_all_share_index()))

    def run(self):
        while True:
            self._print_options()
            try:
                self._input.select_option(self._options).run()
            except _StopError:
                print("Exiting")
                break
            except (StockListError, TradeError) as error:
                print(str(error))
            except _NoStockError:
                pass
