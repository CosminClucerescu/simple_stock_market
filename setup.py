"""Setup script."""
from setuptools import setup

setup(
    setup_requires=['pbr'],
    pbr=True,
)
